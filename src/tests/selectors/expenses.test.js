import getVisibleExpenses from "../../selectors/expenses";
import moment from "moment";
import expenses from "../fixtures/expenses";

test("should filter by text value (case insensetive)", () => {
  const filters = {
    text: "f",
    sortBy: "date",
    startDate: undefined,
    endDate: undefined
  };
  const visibleExpenses = getVisibleExpenses(expenses, filters);
  expect(visibleExpenses).toEqual([expenses[0]]);
});

test("should filter by starting date", () => {
  const filters = {
    text: "",
    sortBy: "date",
    startDate: moment(0),
    endDate: undefined
  };
  const visibleExpenses = getVisibleExpenses(expenses, filters);
  expect(visibleExpenses).toEqual([expenses[2], expenses[0]]);
});

test("should filter by ending date", () => {
  const filters = {
    text: "",
    sortBy: "date",
    startDate: undefined,
    endDate: moment(0)
  };
  const visibleExpenses = getVisibleExpenses(expenses, filters);
  expect(visibleExpenses).toEqual([expenses[0], expenses[1]]);
});

test("should sort by date", () => {
  const filters = {
    text: "",
    sortBy: "date",
    startDate: undefined,
    endDate: undefined
  };
  const visibleExpenses = getVisibleExpenses(expenses, filters);
  expect(visibleExpenses).toEqual([expenses[2], expenses[0], expenses[1]]);
});

test("should sort by amount", () => {
  const filters = {
    text: "",
    sortBy: "amount",
    startDate: undefined,
    endDate: undefined
  };
  const visibleExpenses = getVisibleExpenses(expenses, filters);
  expect(visibleExpenses).toEqual([expenses[0], expenses[1], expenses[2]]);
});
