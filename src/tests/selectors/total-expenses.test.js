import getTotalExpensesAmount from '../../selectors/total-expenses';
import expenses from '../fixtures/expenses'

test('should be 0 if no expenses', () => {
  const totalSum = getTotalExpensesAmount();
  expect(totalSum).toBe(0);
});

test('should sum one expense', () => {
  const totalSum = getTotalExpensesAmount([expenses[0]]);
  expect(totalSum).toBe(expenses[0].amount);
});

test('should sum multiple expenses', () => {
  const totalSum = getTotalExpensesAmount(expenses);
  var manualSum = 0;
  expenses.map((expense) => manualSum += expense.amount);
  expect(totalSum).toBe(manualSum);
})
