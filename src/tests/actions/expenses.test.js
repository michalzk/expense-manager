import { 
  startAdding, 
  addExpense, 
  editExpense, 
  removeExpense, 
  setExpenses,
  startRemoving 
} from "../../actions/expenses";
import expenses from '../fixtures/expenses';
import thunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';
import database from '../../firebase/firebase';

const mockStore = configureMockStore([thunk]);

beforeEach((done) => {
  const expensesData = {};
  expenses.forEach(({ id, description, note, amount, createdAt }) => {
    expensesData[id] = { description, note, amount, createdAt };
  });
  database.ref('expenses').set(expensesData).then(() => done());
})

test("remove expense should return correct action", () => {
  const action = removeExpense({ id: "testID" });
  expect(action).toEqual({
    type: "REMOVE_EXPENSE",
    id: "testID"
  });
});

test("should remove expense from firebase db", (done) => {
  const store = mockStore({});
  const id = expenses[1].id;
  store.dispatch(startRemoving({ id })).then(() => {
    const actions = store.getActions();
    expect(actions[0]).toEqual({
      type: 'REMOVE_EXPENSE',
      id
    });
    return database.ref(`expenses/${id}`).once('value');
  }).then((snapshot) => {
    expect(snapshot.val()).toBeFalsy();
    done();
  });
})

test("edit expense should return correct action", () => {
  const action = editExpense("testID", { amount: 100.0 });
  expect(action).toEqual({
    type: "EDIT_EXPENSE",
    id: "testID",
    updates: { amount: 100.0 }
  });
});

test("add expense should return correct action (custom values)", () => {
  const action = addExpense(expenses[1]);
  expect(action).toEqual({
    type: "ADD_EXPENSE",
    expense: expenses[1]
  });
});

test('should add expense to db and store', (done) => {
  const store = mockStore({});
  const expenseData = { 
    description: 'Test',
    amount: 300,
    note: 'This is test case 1',
    createdAt: 1000
  };
  store.dispatch(startAdding(expenseData)).then(() => {
    const actions = store.getActions();
    expect(actions[0]).toEqual({
      type: 'ADD_EXPENSE',
      expense: {
        id: expect.any(String),
        ...expenseData
      }
    });
    return database.ref(`expenses/${actions[0].expense.id}`).once('value');
  }).then((snapshot) => { 
    expect(snapshot.val()).toEqual(expenseData);
    done();
  });
});

test('should add expense to db and store(defaults)', (done) => {
  const store = mockStore({});
  const expenseData = { 
    description: '',
    amount: 0,
    note: '',
    createdAt: 0
  };
  store.dispatch(startAdding({})).then(() => {
    const actions = store.getActions();
    expect(actions[0]).toEqual({
      type: 'ADD_EXPENSE',
      expense: {
        id: expect.any(String),
        ...expenseData
      }
    });
    return database.ref(`expenses/${actions[0].expense.id}`).once('value');
  }).then((snapshot) => { 
    expect(snapshot.val()).toEqual(expenseData);
    done();
  });
});

test('should setup set expense action object with data', () => {
  const action = setExpenses(expenses);
  expect(action).toEqual({
    type: 'SET_EXPENSES',
    expenses
  });
});