import React from 'react';
import { shallow } from 'enzyme';
import expenses from '../fixtures/expenses';
import { EditExpensePage } from '../../components/EditExpensePage';

let editExpense, startRemoving, history, wrapper;

beforeEach(() => {
  editExpense = jest.fn();
  startRemoving = jest.fn();
  history = { push: jest.fn() };
  wrapper = shallow(
    <EditExpensePage 
      editExpense={editExpense} 
      startRemoving={startRemoving} 
      history={history} 
      expense={expenses[2]}
    />
  );
})

test('should render EditExpensePage', () => { 
  expect(wrapper).toMatchSnapshot();
});

test('should call editExpense', () => {
  wrapper.find('ExpenseForm').prop('onSubmit')(expenses[2])
  expect(history.push).toHaveBeenCalledWith('/');
  expect(editExpense).toHaveBeenLastCalledWith(
    expenses[2].id,
    expenses[2]
  );
});

test('should call startRemoving', () => {
  wrapper.find('button').simulate('click');
  expect(history.push).toHaveBeenCalledWith('/');
  expect(startRemoving).toHaveBeenLastCalledWith({
    id: expenses[2].id
  });
});