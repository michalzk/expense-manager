import React from 'react';
import { shallow } from 'enzyme';
import { ExpensesSummary } from '../../components/ExpensesSummary';

test('should render correctly for one expense', () => {
  const wrapper = shallow(<ExpensesSummary expensesLength={1} totalAmount={235.12} />);
  expect(wrapper).toMatchSnapshot();
});

test('should render correctly for multiple expenses', () => {
  const wrapper = shallow(<ExpensesSummary expensesLength={11} totalAmount={232125.12} />);
  expect(wrapper).toMatchSnapshot();
});

test('should render correctly for no expenses', () => {
  const wrapper = shallow(<ExpensesSummary expensesLength={0} totalAmount={0} />);
  expect(wrapper).toMatchSnapshot();
})