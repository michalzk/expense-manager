import React from 'react';
import { shallow } from 'enzyme';
import { AddExpensePage } from '../../components/AddExpensePage';
import expenses from '../fixtures/expenses';

let startAdding, history, wrapper;

beforeEach(() => {
  startAdding = jest.fn();
  history = { push: jest.fn() };
  wrapper = shallow(<AddExpensePage startAdding={startAdding} history={history}/>);
})

test('should render add expense page', () => {
  expect(wrapper).toMatchSnapshot();
});

test('should call onSubmit', () => {
  wrapper.find('ExpenseForm').prop('onSubmit')(expenses[1]);
  expect(history.push).toHaveBeenCalledWith('/');
  expect(startAdding).toHaveBeenLastCalledWith(expenses[1]);
});