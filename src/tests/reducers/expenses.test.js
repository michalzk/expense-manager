import expensesReducer from "../../reducers/expenses";
import moment from "moment";
import expenses from "../fixtures/expenses";

test("should start with default values", () => {
  const state = expensesReducer(undefined, { type: "@@INIT" });
  expect(state).toEqual([]);
});

test("should remove expense by given ID", () => {
  const action = {
    type: "REMOVE_EXPENSE",
    id: expenses[1].id
  };
  const state = expensesReducer(expenses, action);
  expect(state).toEqual([expenses[0], expenses[2]]);
});

test("should not remove any expense if ID is not found", () => {
  const action = {
    type: "REMOVE_EXPENSE",
    id: "-01"
  };
  const state = expensesReducer(expenses, action);
  expect(state).toEqual(expenses);
});

test("should correctly add an expense", () => {
  const expense = {
    id: "11",
    description: "test",
    note: "",
    amount: 12.01,
    createdAt: moment(0)
  };
  const action = {
    type: "ADD_EXPENSE",
    expense
  };
  const state = expensesReducer(expenses, action);
  expect(state).toEqual([...expenses, expense]);
});

test("should correctly edit an expense", () => {
  const updates = {
    amount: 12.01
  };
  const action = {
    type: "EDIT_EXPENSE",
    id: expenses[0].id,
    updates
  };
  const state = expensesReducer(expenses, action);
  expect(state[0].amount).toBe(updates.amount);
});

test("should not edit an expense without proper ID", () => {
  const updates = {
    amount: 12.01
  };
  const action = {
    type: "EDIT_EXPENSE",
    id: "uaaaa",
    updates
  };
  const state = expensesReducer(expenses, action);
  expect(state).toEqual(expenses);
});

test('should set expenses', () => {
  const action = {
    type: 'SET_EXPENSES',
    expenses: [expenses[1]]
  };
  const state = expensesReducer(expenses, action);
  expect(state).toEqual([expenses[1]]);
});