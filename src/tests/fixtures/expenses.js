import moment from "moment";

export default [
  {
    id: "1",
    description: "First",
    note: "expense made for test purposes",
    amount: 123901.12,
    createdAt: 0
  },
  {
    id: "2",
    description: "Second",
    note: "",
    amount: 123.1,
    createdAt: moment(0)
      .subtract(1, "days")
      .valueOf()
  },
  {
    id: "3",
    description: "Third",
    note: "",
    amount: 0.12,
    createdAt: moment(0)
      .add(1, "days")
      .valueOf()
  }
];
