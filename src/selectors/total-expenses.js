export default (expenses = []) => {
  const total = expenses.length === 0
    ? 0
    : expenses.reduce((sum, ex) => sum + ex.amount, 0)
  return total;
}
