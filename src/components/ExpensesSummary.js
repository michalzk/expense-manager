import React from 'react';
import numeral from 'numeral';
import { connect } from 'react-redux';
import selectExpenses from '../selectors/expenses';
import totalExpensesAmount from '../selectors/total-expenses'; 
import "numeral/locales/pl";

numeral.locale('pl');

export const ExpensesSummary = ({expensesLength, totalAmount}) => {
  const countInflection = (
    expensesLength === 1
      ? 'expense'
      : 'expenses'
  );
  const formattedAmount = numeral(totalAmount).format('0,0[.]00 $')
  return (
    <div>
      <h1>
        You are now viewing
        {' ' + expensesLength + ' ' + countInflection}
        {' totalling '} 
        {formattedAmount}
      </h1>
    </div>
  )
}

const mapStateToProps = (state) => {
  const visibleExpenses = selectExpenses(state.expenses, state.filters);
  return {
    expensesLength: visibleExpenses.length,
    totalAmount: totalExpensesAmount(visibleExpenses)
  }
}

export default connect(mapStateToProps)(ExpensesSummary);