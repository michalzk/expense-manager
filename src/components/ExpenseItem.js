import React from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';
import numeral from 'numeral';
import "numeral/locales/pl";

numeral.locale('pl');

const ExpenseItem = ({ id, description, note, amount, createdAt }) => (
  <div>
    <Link to={`/edit/${id}`}>
      <h3>{description}</h3>
    </Link>
    <p>
      {numeral(amount).format('0,0[.]00 $')}
      {' - '}
      {moment(createdAt).format('DD-MM-YYYY')}
    </p>
    <p>{note}</p>
  </div>
);

export default ExpenseItem;
