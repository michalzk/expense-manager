import React from 'react';
import { connect } from 'react-redux';
import ExpenseForm from './ExpenseForm';
import { editExpense, startRemoving } from '../actions/expenses';

export class EditExpensePage extends React.Component {
  onEdit = (expense) => {
    this.props.editExpense(this.props.expense.id, expense);
    this.props.history.push("/");
  };
  onRemove = () => {
    this.props.startRemoving({ id: this.props.expense.id });
    this.props.history.push("/");
  };
  render() {
    return (
      <div>
        Editing (ID): {this.props.expense.id}
        <ExpenseForm
          expense={this.props.expense}
          onSubmit={this.onEdit}
        />{" "}
        <button
          onClick={this.onRemove}
        >
          Delete
        </button>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  expense: state.expenses.find(
    expense => expense.id === props.match.params.id
  )
});

const mapDispatchToProps = (dispatch, props) => ({
  editExpense: (id, expense) => dispatch(editExpense(id, expense)),
  startRemoving: (data) => dispatch(startRemoving(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(EditExpensePage);
